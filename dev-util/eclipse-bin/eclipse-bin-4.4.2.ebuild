# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit eutils java-pkg-2

DESCRIPTION="Eclipse SDK"
HOMEPAGE="http://eclipse.org/"
BASE_URI="http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/luna/SR2/eclipse-java-luna-SR2-linux-gtk"
SRC_URI="amd64? ( ${BASE_URI}-x86_64.tar.gz&r=1 -> ${P}-amd64.tar.gz ) x86? ( ${BASE_URI}.tar.gz&r=1 -> ${P}-i386.tar.gz )"

LICENSE="EPL-1.0"
SLOT="4.4"
KEYWORDS="-* amd64 x86"
RESTRICT="mirror"

RDEPEND=">=virtual/jdk-1.7 x11-libs/gtk+:2"
DEPEND="${RDEPEND}"

S=${WORKDIR}/eclipse

src_unpack() {
	unpack ${A}
}

src_install() {
	local dest=/opt/${PN}-${SLOT}
	insinto ${dest}
	doins -r features icon.xpm plugins artifacts.xml p2 eclipse.ini configuration dropins
	exeinto ${dest}
	doexe eclipse
	insinto /etc
	doins "${FILESDIR}/eclipserc-bin-${SLOT}"
	dobin "${FILESDIR}/eclipse-bin-${SLOT}"
	make_desktop_entry "eclipse-bin-${SLOT}" "Eclipse ${PV} (bin)" "${dest}/icon.xpm"
}
