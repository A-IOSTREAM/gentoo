# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit eutils

DESCRIPTION="Intelligent Python IDE with unique code assistance and analysis"
HOMEPAGE="http://www.jetbrains.com/pycharm/"
SRC_URI="https://download.jetbrains.com/python/${P}.tar.gz"

LICENSE="Apache-2.0 BSD CDDL MIT-with-advertising"
SLOT="5.0"
KEYWORDS="-* amd64 x86"
IUSE=""
RESTRICT="mirror strip"
QA_PREBUILT="/opt/${PN2}-${SLOT}/bin/fsnotifier
	/opt/${PN2}-${SLOT}/bin/fsnotifier64
	/opt/${PN2}-${SLOT}/bin/fsnotifier-arm"

RDEPEND=">=virtual/jre-1.7"
DEPEND="${RDEPEND}"

PN2="${PN/-community/}"

src_unpack() {
	unpack ${A}
}

src_install() {
	local dest=/opt/${PN2}-${SLOT}
	insinto ${dest}
	doins -r *
	fperms a+x ${dest}/bin/{pycharm.sh,fsnotifier{,64},inspect.sh}
	dosym ${dest}/bin/pycharm.sh /usr/bin/${PN2}-${SLOT}
	newicon "bin/${PN2}.png" ${PN}-${SLOT}.png
	make_desktop_entry "${PN2}-${SLOT}" "PyCharm ${PV} (bin)" "${PN}-${SLOT}" "Development"
}
