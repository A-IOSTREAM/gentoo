# (c) Andrey Lukyanenko (A. IOSTREAM) 2016-2018
# Distributed under terms of MIT licanse (aka Expat license)
# The full text is located in the LICENSE file at the repository root

EAPI=2

DESCRIPTION="A barcode encoding library with Qt frontend"
HOMEPAGE="https://zint.github.io/"
SRC_URI="mirror://sourceforge/zint/${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="mirror"

RDEPEND="dev-qt/designer:4 dev-qt/qtgui:4 media-libs/libpng sys-libs/zlib"
DEPEND="${RDEPEND} dev-util/cmake"

inherit eutils

src_unpack() {
	unpack ${A}
	cd "${S}"
}

src_configure() {
	cmake -D CMAKE_INSTALL_PREFIX="/usr" "${S}"
}

src_compile() {
	emake || die "Build error occurred"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install error occurred"
	doicon "${S}/zint.png"
	make_desktop_entry zint-qt "Zint Barcode Studio" /usr/share/pixmaps/zint.png Graphics
}
