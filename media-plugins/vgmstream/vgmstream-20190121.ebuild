# (c) Andrey Lukyanenko (A. IOSTREAM) 2015-2019
# Distributed under terms of MIT licanse (aka Expat license)
# The full text is located in the LICENSE file at the repository root

EAPI=2

DESCRIPTION="A library for playback of various streamed audio formats used in video games"
HOMEPAGE="http://hcs64.com/vgmstream.html"
DISTROOT="https://iostream.by/dists/gentoo"
SRC_URI="${DISTROOT}/${P}.tgz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RESTRICT="mirror"

RDEPEND="
	>=media-sound/audacious-3.6
	media-libs/libvorbis
	media-sound/mpg123
"
DEPEND="${RDEPEND}"

inherit eutils autotools xdg-utils

src_unpack() {
	unpack ${A}
	cd "${S}"
}

src_prepare() {
	# this was copied from bootstrap script in order not to mess with eautoreconf
	VGMSTREAM_SRCS=`(cd ./src/ && ls *.c) | tr '\n' ' '`
	VGMSTREAM_HDRS=`(cd ./src/ && ls *.h) | tr '\n' ' '`
	CODING_SRCS=`(cd ./src/coding/ && ls *.c) | tr '\n' ' '`
	CODING_HDRS=`(cd ./src/coding/ && ls *.h) | tr '\n' ' '`
	LAYOUT_SRCS=`(cd ./src/layout/ && ls *.c) | tr '\n' ' '`
	LAYOUT_HDRS=`(cd ./src/layout/ && ls *.h) | tr '\n' ' '`
	META_SRCS=`(cd ./src/meta/ && ls *.c) | tr '\n' ' '`
	META_HDRS=`(cd ./src/meta/ && ls *.h) | tr '\n' ' '`
	AUDACIOUS_SRCS=`(cd ./audacious/ && ls *.cc) | tr '\n' ' '`
	AUDACIOUS_HDRS=`(cd ./audacious/ && ls *.h) | tr '\n' ' '`
	sed -i -e "s/libvgmstream_la_SOURCES =.*/libvgmstream_la_SOURCES = $VGMSTREAM_SRCS/g" ./src/Makefile.autotools.am
	sed -i -e "s/EXTRA_DIST =.*/EXTRA_DIST = $VGMSTREAM_HDRS/g" ./src/Makefile.autotools.am
	sed -i -e "s/libcoding_la_SOURCES =.*/libcoding_la_SOURCES = $CODING_SRCS/g" ./src/coding/Makefile.autotools.am
	sed -i -e "s/EXTRA_DIST =.*/EXTRA_DIST = $CODING_HDRS/g" ./src/coding/Makefile.autotools.am
	sed -i -e "s/liblayout_la_SOURCES =.*/liblayout_la_SOURCES = $LAYOUT_SRCS/g" ./src/layout/Makefile.autotools.am
	sed -i -e "s/EXTRA_DIST =.*/EXTRA_DIST = $LAYOUT_HDRS/g" ./src/layout/Makefile.autotools.am
	sed -i -e "s/libmeta_la_SOURCES =.*/libmeta_la_SOURCES = $META_SRCS/g" ./src/meta/Makefile.autotools.am
	sed -i -e "s/EXTRA_DIST =.*/EXTRA_DIST = $META_HDRS/g" ./src/meta/Makefile.autotools.am
	sed -i -e "s/libvgmstream_la_SOURCES =.*/libvgmstream_la_SOURCES = $AUDACIOUS_SRCS/g" ./audacious/Makefile.autotools.am
	sed -i -e "s/EXTRA_DIST =.*/EXTRA_DIST = $AUDACIOUS_HDRS/g" ./audacious/Makefile.autotools.am

	# use $PV instead of git revision
	sed -i -e "s/VGMSTREAM_VERSION/${PV}/g" ./audacious/Makefile.autotools.am

	eautoreconf
}

src_compile() {
	emake -f Makefile.autotools || die "Build error occurred"
}

src_install() {
	emake DESTDIR="${D}" -f Makefile.autotools install || die "Install error occurred"
	insinto /usr/share/mime/packages
	doins "${FILESDIR}/vgmstream.xml"
}

pkg_postinst() {
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_mimeinfo_database_update
}

