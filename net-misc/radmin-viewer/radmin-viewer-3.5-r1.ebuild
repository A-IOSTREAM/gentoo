# (c) Andrey Lukyanenko (A. IOSTREAM) 2015-2018
# Distributed under terms of MIT licanse (aka Expat license)
# The full text is located in the LICENSE file at the repository root

EAPI=5

inherit eutils multilib

DESCRIPTION="Radmin remote access client"
HOMEPAGE="http://www.radmin.com/radmin/"
SRC_URI="http://www.radmin.com/download/rviewer35exe.zip"

LICENSE="Radmin-EULA"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="mirror"

RDEPEND="virtual/wine[abi_x86_32]"
DEPEND="${RDEPEND}"

S="${WORKDIR}/Radmin Viewer 3"

src_unpack() {
	unpack ${A}
}

src_install() {
	local dest=/opt/${PN}
	insinto ${dest}
	doins -r *
	newicon "${FILESDIR}/Radmin.png" ${PN}.png
	make_desktop_entry "wine /opt/${PN}/Radmin.exe" "Radmin Viewer (win32)" "${PN}" "Network"
}
