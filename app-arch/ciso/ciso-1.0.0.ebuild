# (c) Andrey Lukyanenko (A. IOSTREAM) 2014-2018
# Distributed under terms of MIT licanse (aka Expat license)
# The full text is located in the LICENSE file at the repository root

EAPI=2

DESCRIPTION="Sony PSP-compatible ISO compressor"
HOMEPAGE="http://sourceforge.net/projects/ciso/"
SRC_URI="mirror://sourceforge/ciso/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="mirror"

RDEPEND="sys-libs/zlib"
DEPEND="${RDEPEND}"

inherit eutils

src_unpack() {
	unpack ${A}
	cd "${S}"
}

src_prepare() {
	epatch "${FILESDIR}/${P}-fix_make_install.patch" || die "Patch error occurred"
}

src_compile() {
	emake || die "Build error occurred"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install error occurred"
}
