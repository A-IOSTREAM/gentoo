# (c) Andrey Lukyanenko (A. IOSTREAM) 2015-2018
# Distributed under terms of MIT licanse (aka Expat license)
# The full text is located in the LICENSE file at the repository root

EAPI=2

DESCRIPTION="A legacy D-Bus client library that used to be included with Audacious"
HOMEPAGE="http://audacious-media-player.org/"
SRC_URI="http://distfiles.audacious-media-player.org/${P}-rc2.tar.bz2"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RESTRICT="mirror"

RDEPEND=">=media-sound/audacious-3.4.3"
DEPEND="${RDEPEND}"

S=${WORKDIR}/${P}-rc2

inherit eutils multilib autotools

src_unpack() {
	unpack ${A}
	cd "${S}"
}

src_prepare() {
	eautoreconf
}

src_compile() {
	emake || die "Build error occurred"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install error occurred"
}
