# (c) Andrey Lukyanenko (A. IOSTREAM) 2016-2018
# Distributed under terms of MIT licanse (aka Expat license)
# The full text is located in the LICENSE file at the repository root

EAPI=2

DESCRIPTION="Open-source Nintendo Wii Remote Linux device driver"
HOMEPAGE="http://dvdhrm.github.io/xwiimote/"
DISTROOT="https://iostream.by/dists/gentoo"
SRC_URI="${DISTROOT}/${P}.tgz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RESTRICT="mirror"

RDEPEND=">=net-wireless/bluez-4.01 net-wireless/blueman"
DEPEND="${RDEPEND}"

inherit eutils autotools linux-info

pkg_setup() {
	local CONFIG_CHECK="~HID_WIIMOTE"
	linux-info_pkg_setup
}

src_unpack() {
	unpack ${A}
	cd "${S}"
}

src_prepare() {
	eautoreconf
}

src_compile() {
	emake || die "Build error occurred"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install error occurred"
}

pkg_postinst() {
	elog "You must restart the Bluetooth daemon by running:"
	elog "# /etc/init.d/bluetooth restart"
}
